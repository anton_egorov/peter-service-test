<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
    <link rel='stylesheet' href='/webjars/bootstrap/3.3.7/css/bootstrap.min.css'>

    <script type="text/javascript" src="/webjars/jquery/3.1.0/jquery.min.js"></script>
    <script type="text/javascript" src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="/webjars/sockjs-client/1.0.2/sockjs.min.js"></script>
    <script type="text/javascript" src="/webjars/stomp-websocket/2.3.3/stomp.min.js"></script>
    <script type="text/javascript" src="/resources/app.js"></script>

</head>
<body>


<div class="container">
    <form id="logout" action="/logout" method="post">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    </form>
    <c:if test="${pageContext.request.userPrincipal.name != null}">
        <div class="row">
            <div class="col-sm-2"><label>User : ${pageContext.request.userPrincipal.name}</label></div>
            <div class="col-sm-offset-9 col-sm-1">
        <a href="javascript:document.getElementById('logout').submit()">Logout
        </a>
                </div>
        </div>
    </c:if>
    <div class="panel panel-primary">
        <div class="panel-heading"> Units</div>
        <div class="panel-body">

            <div class="row">
                <div class="col-lg-12">
                    <div class="input-group">

                        <input id="description" name="description" type="text" class="form-control"
                               placeholder="Description...">
                        <span class="input-group-btn">
                            <button class="btn btn-default" onclick="addUnit()">Add</button>
                        </span>


                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Description</th>
                        <th>Value</th>
                        <th>User</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody id="units-table"/>
                </table>
            </div>
        </div>
    </div>
</div>


</body>
</html>