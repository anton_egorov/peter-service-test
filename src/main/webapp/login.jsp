<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<html>
<head>
    <link rel='stylesheet' href='/webjars/bootstrap/3.3.7/css/bootstrap.min.css'>
    <link href="${contextPath}/resources/common.css" rel="stylesheet">
</head>
<body>


<%--<c:if test="${not empty error}">--%>
    <%--<div>${error}</div>--%>
<%--</c:if>--%>
<%--<c:if test="${not empty message}">--%>
    <%--<div>${message}</div>--%>
<%--</c:if>--%>

<div class="container">
    <form method="POST" action="${contextPath}/login" class="form-signin">
    <%--<form name='login' action="<c:url value='/login' />" method='POST' class="form-singin">--%>
        <h2 class="form-heading">Log in</h2>
        <div class="form-group ${error != null ? 'has-error' : ''}">
            <span>${message}</span>
            <input name="username" type="text" class="form-control" placeholder="Username"
                   autofocus="true"/>
            <input name="password" type="password" class="form-control" placeholder="Password"/>
            <span>${error}</span>
            <%--<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>--%>

            <button class="btn btn-lg btn-primary btn-block" type="submit">Log In</button>
        </div>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    </form>
</div>
</body>
</html>