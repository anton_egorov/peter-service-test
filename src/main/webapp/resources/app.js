var host = "http://" + window.location.hostname + ":" + window.location.port;

(function () {
    var socket = new SockJS('/app-websocket');
    var stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        console.log('Connected: ' + frame);
        stompClient.subscribe('/app/data-updated', function () {
            console.log("Receive Data-Updated Event");
            getUnits();
        });
    });

    getUnits();


})();




function addUnit() {
    var description = $("#description").val();

    if (typeof description == 'undefined' || description == '' || description == null) {
        alert("Enter Description");
    } else {

        var url = host + "/units";
        $.ajax({
            method: 'POST',
            url: host + "/units",
            data:{description:description},
            success : function() {
                $("#description").val("");
                console.log("Send Add Unit");
            },
            error : function ( error ) {
                alert("Error: " + JSON.parse(error.responseText).message);
            }
        });
        // $.post( url, {description: description}).done(function () {
        //     console.log("Send Add Unit");
        // })

    }
}

function increment(id) {
    $.ajax({
        method: 'PUT',
        url: host + "/units/" + id +"/increment",
        success : function() {
            console.log("Send Increment for " + id)
        },
        error : function ( error ) {
            alert("Error: " + JSON.parse(error.responseText).message);
        }
    });
}

function decrement(id) {
    $.ajax({
        method: 'PUT',
        url: host + "/units/" + id +"/decrement",
        success : function() {
            console.log("Send Decrement for " + id)
        },
        error : function ( error ) {
            alert("Error: " + JSON.parse(error.responseText).message);
        }
    });
}

function getUnits() {
    // $.get(
    //     host + "/units",
    //     function (result) {
    //         redraw(result);
    //     }).done( function(){
    //     console.log("Send Get Units");
    // });
    $.ajax({
        method: 'GET',
        url: host + "/units/",
        success : function( result ) {
            console.log("Send Get Units");
            redraw(result);
        },
        error : function ( error ) {
            alert("Error: " + JSON.parse(error.responseText).message);
        }
    });

}

function redraw(data) {
    $("#units-table").empty();
    console.log("Redraw Units Table")
    $.each(data, function () {
        $("#units-table ").append("<tr><td>" + this.id + "</td><td>"
            + this.description + "</td><td>" + this.value + "</td><td>"
            + this.userName + "</td>" +
            "<td><button class=\"btn btn-xs\" onclick=\"increment(" + this.id + ")\">+</button></td>" +
            "<td><button class=\"btn btn-xs\" onclick=\"decrement(" + this.id + ")\">-</button></td></tr>");
    });


}
