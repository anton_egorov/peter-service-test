package ru.egorov.app.controller;

/**
 * Created by anton on 17.01.2017.
 */
public interface SessionService {
    String getCurrentUserName();
}
