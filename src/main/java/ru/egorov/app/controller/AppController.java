package ru.egorov.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.egorov.app.model.Unit;
import ru.egorov.app.storage.UnitStorage;

import java.util.List;

/**
 * Created by anton on 17.01.2017.
 */
@Controller
public class AppController {

    private final SessionService sessionService;

    private final UnitStorage storage;

    @Autowired
    AppController( SessionService sessionService, UnitStorage unitStorage ){
        this.sessionService = sessionService;
        this.storage = unitStorage;
    }


    @GetMapping( path = "units" )
    public @ResponseBody  List< Unit > getUnits(){
        return storage.getUnits();
    }

    @Secured( {"ROLE_ADMIN", "ROLE_SUPPORT"})
    @PostMapping( path = "units" )
    public String add( @RequestParam  String description ){
        storage.add( description,sessionService.getCurrentUserName() );
        return "redirect:/";
    }

    @Secured( {"ROLE_ADMIN", "ROLE_SUPPORT"})
    @PutMapping( path = "/units/{id}/increment" )
    @ResponseStatus(HttpStatus.OK)
    public void increment( @PathVariable int id ){
        storage.increment( id, sessionService.getCurrentUserName() );
    }

    @Secured( {"ROLE_ADMIN", "ROLE_SUPPORT"})
    @PutMapping( path = "/units/{id}/decrement" )
    @ResponseStatus(HttpStatus.OK)
    public void decrement( @PathVariable int id ){
        storage.decrement(id, sessionService.getCurrentUserName());
    }


    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model, String error, String logout) {
        if (error != null) {
            model.addAttribute("error", "Your username and password is invalid.");
            return "login";
        }
        if (logout != null) {
            model.addAttribute("message", "You have been logged out successfully.");
            return "login";
        }
        return "login";
    }

    @RequestMapping(value = {"/", "/main"}, method = RequestMethod.GET)
    public String welcome(Model model) {
        return "main";
    }

}

