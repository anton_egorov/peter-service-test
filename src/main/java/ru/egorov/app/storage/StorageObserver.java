package ru.egorov.app.storage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;
import ru.egorov.app.model.Unit;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by anton on 17.01.2017.
 */
@Component
public class StorageObserver implements Observer{

    @Autowired
    SimpMessagingTemplate template;

    @Override
    public void update(Observable o, Object arg) {
        notifyUi( );
    }

    public void notifyUi(  ) {
        template.convertAndSend("/app/data-updated", "refresh");
    }
}
