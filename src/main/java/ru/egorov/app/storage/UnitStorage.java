package ru.egorov.app.storage;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.egorov.app.model.Unit;

import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Created by anton on 17.01.2017.
 */
@Slf4j
@Component
public class UnitStorage extends Observable {

    private final List<Unit> units = new CopyOnWriteArrayList<>();

    private final static AtomicInteger increment = new AtomicInteger(0);

    private ReentrantLock lock = new ReentrantLock();


    @Autowired
    public UnitStorage( Observer observer ) {
        addObserver(observer);
    }

    Unit getById(int id ){
        for (Unit unit : units) {
            if( unit.getId().equals(id) )
                return unit;
        }
        return null;
    }

    public void add( String description , String username ){
        units.add( new Unit( increment.incrementAndGet(), new AtomicInteger(0), description, username ));
        log.debug("Added unit description {} by {}", description, username);
        sendNotify();
    }

    public int increment( int id, String username ){
        Unit unit = getById(id);
        if( unit == null )
            return -1;
        lock.lock();
            unit.setUserName(username);
            int newValue = unit.getValue().incrementAndGet();
        lock.unlock();
        log.debug("increment unit {} by {}", id, username);
        sendNotify();
        return newValue;
    }

    public int decrement( int id, String username  ){
        Unit unit = getById(id);
        if( unit == null )
            return -1;
        lock.lock();
        unit.setUserName(username);
        int newValue = unit.getValue().decrementAndGet();
        lock.unlock();
        log.debug("decrement unit {} by {}", id, username);
        sendNotify();
        return newValue;
    }

    public List<Unit> getUnits() {
        return units;
    }

    private void sendNotify(){
        setChanged();
        notifyObservers();
    }
}
