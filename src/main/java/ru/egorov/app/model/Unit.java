package ru.egorov.app.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by anton on 17.01.2017.
 */
@NoArgsConstructor
@AllArgsConstructor
public class Unit {

    @Getter
    private Integer id;

    @Getter
    @Setter
    private AtomicInteger value;

    @Getter
    @Setter
    private String description;

    @Getter
    @Setter
    private String userName;
}
